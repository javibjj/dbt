--dropping cloned database:
drop database if exists EDW_STG;

--creating zero copy clone of PROD database
create database EDW_STG clone EDW_DEV;

--granting "USAGE" privilege to cloned Database
grant usage on database EDW_STG to role SVC_RO_DEV_ROLE;
grant usage on database EDW_STG to role SVC_RW_DEV_ROLE;
