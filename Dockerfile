# Base image:
FROM python:3.8

# Copying the requirement file:
COPY requirements.txt ./

# Installing Python Requirements:
RUN pip install -U pip
RUN pip install -r requirements.txt
#RUN pip3 install boto3

# Installing VIM and Bash completion:
RUN apt-get update
RUN apt-get install -y vim
RUN apt-get install -y bash-completion

# Installing dbt completion script:
RUN curl https://raw.githubusercontent.com/fishtown-analytics/dbt-completion.bash/master/dbt-completion.bash > ~/.dbt-completion.bash
RUN /bin/bash -c "source ~/.dbt-completion.bash"
RUN echo 'source ~/.dbt-completion.bash' >> ~/.bashrc

# Setting DBT_Home directory:
RUN mkdir /home/dbt
RUN mkdir /home/dbt/lakehouse
ENV DBT_HOME=/home/dbt
ENV DBT_lakehouse=/home/dbt/lakehouse

# Setting the DBT_PROFILES directory:
RUN mkdir $DBT_lakehouse/profile
ENV DBT_PROFILES_DIR=$DBT_lakehouse/profile

# Copying the necessary scripts:
COPY ./entrypoint.sh /
COPY ./retrieve_secrets.py $DBT_HOME
COPY ./extract_json_key.py $DBT_HOME
COPY ./profiles.yml $DBT_PROFILES_DIR

# Setting PYTHONUNBUFFERED to non-empty value:
ENV PYTHONUNBUFFERED=1

# Setting permission to Entrypoint script:
RUN chmod 700 entrypoint.sh \
      && pip install awscli

# Executing the entrypoint script:
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
CMD [""]
