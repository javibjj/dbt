import boto3
import base64
import os ,sys, json
from botocore.exceptions import ClientError

def get_secret():
    secret_name = sys.argv[1]
    region_name = "us-east-1"

# Creating a Secrets Manager client:
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:        
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name            
        )
        
    except ClientError as e:        
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            print("The request had Decryption failure:", e)
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            print("The request had Internal service error:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
            
    else:
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret_data = get_secret_value_response['SecretString']
        else:
            secret_data = base64.b64decode(get_secret_value_response['SecretBinary'])
                        
    return secret_data

if __name__ == "__main__":       
    secret_data = get_secret()
    print("__SECRETS__ " + secret_data)
