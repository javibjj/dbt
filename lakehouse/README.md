Welcome to your new dbt project!

# Overview
This framework help us to transform data simply writing sql statements for different data ingestion pipelines and feed the data into our warehouse.

# Installation
Follow steps from below confluence page to install DBT. This explains DBT Installation for both Mac and Window OS.
https://bhsf.atlassian.net/wiki/spaces/TACMP/pages/411730633/How+to+Install+DBT

Please note that it is required to provide appropriate version information for dbt-vault under packages.yml file. Please use below link for additional version details.

https://dbtvault.readthedocs.io/en/latest/versions/

Prerequisite for the current dbt project setup - 

python version : 3.10.5

dbt version : 1.1.0

dbt vault version : 0.8.3

# Profile Configuration
Run below command to identify the location for profile.yml and provide Snowflake account details like account_name, username, password, database, schema, threads etc.
```
dbt debug --config-dir
```

# Project File
dbt_project.yml is the main project file which stores all the information related to data models, global variables, macros etc.

Profile information in this file should be matched with the profile name present in dbt_profile.yml file.

```
name: dbtvault_snowflake_demo
profile: dbtvault_snowflake_demo
version: '5.0.0'
config-version: 2

analysis-paths:
  - analysis
clean-targets:
  - target
data-paths:
  - data
macro-paths:
  - macros
source-paths:
  - models
test-paths:
  - tests
target-path: target

vars:
  load_date: '1992-01-08'
  tpch_size: 10 #1, 10, 100, 1000, 10000

models:
  default_project:
      tags:
        - 'default_project'
      raw_stage:
        tags:
          - 'raw'
        materialized: view
      stage:
        tags:
          - 'stage'
        enabled: true
        materialized: view
      raw_vault:
        tags:
          - 'raw_vault'
        materialized: incremental
        hubs:
          tags:
            - 'hubs'
        links:
          tags:
            - 'links'
        sats:
          tags:
            - 'satellites'
        t_links:
          tags:
            - 't_links'
```

# Project Design
Please follow below folder structure and naming convention for new dbt project. For illustration purpose we have created default_project under models folder.

- raw_stage -> First create Raw-Staging-Layer to maintain raw data as well as we can write transformation queries applying multiple joins. Run below command to build this layer.
```
dbt run -s tag:raw
```
- stage -> This staging layer needs additional columns to prepare data for loading to the raw vault. Specifically we add key hashes, hashdiffs, fixed value columns like record_source and load_date. Run below command to build this layer.
```
dbt run -s tag:stage
```
- raw_vault -> We use Hubs, Links and Satellites folder structure for loading the vault.
  - Hub -> In hubs we maintain business_key / Primary_key , Natural_key, load_datetime, record_source. All the files under hubs folder should start with "H_".
    ```
    dbt run -s tag:hub
    ```
  - Links -> Link is used to connect Hubs. We maintain Primary_key, Foreign_Key, load_datetime and record_source. All the files under links folder should start with "L_".
    ```
    dbt run -s tag:links
    ```
  - Satellite -> We maintains point-in-time payload data related to parent Hub or Links records. We maintains Primary_key, HashDiff, Payload (additional attributes for an entity), load_datetime and record_source. All the files under links folder should start with "S_".
    ```
    dbt run -s tag:satellites
    ```
- Sources.yml - For each project we will maintain this file to maintain source information, which we can use in our main project via jinja template. Example
```
{{ source('tpch_sample', 'PARTSUPP') }}
```

# Loading the Full System
Irrespective or running each layer separately we can run full system using below command.
```
dbt run --full-refresh
```

### Resources:
- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](https://community.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices
