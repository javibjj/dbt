with drname as (
    select * from drname where LENGTH('dr_no_ext') = 6 and REGEXP_LIKE('dr_no_ext','^[0-9]*$') and dr_lname != 'Test'
),

with grouptbl as (
   select x.dr_id,
    x.dr_no_ext as provider_no,
    x.group_no as group_cd,
    x.dr_lname  as group_desc,
    x.sch_type,
    case when x.sch_type = '1' then 'Y' else 'N'  end as Primary_practice_ind
   from (
        SELECT DISTINCT
         od.dr_id as dr_id,  da1.sch_type,
         LTRIM(RTRIM(dd2.dr_no_ext)) as dr_no_ext,
         ISNULL(RTRIM(od.dr_no_ext),'') as group_no,
         LTRIM(RTRIM(od.dr_lname)) as dr_lname
        FROM drname od
          join address1 oa1
          ON od.dr_id = oa1.dr_id
          JOIN address2 oa2 ON oa1.link = oa2.l_address
          JOIN address2 da2 ON oa2.tax_id = da2.tax_id
          JOIN address1 da1 ON da2.l_address = da1.link
          JOIN drname dd2 on da1.dr_id = dd2.dr_id
          WHERE od.verfac = 'true'
          AND upper(da1.sch_type) NOT IN ('RUO', 'BUS', '13')
          AND ISNUMERIC(dd2.dr_no_ext)=1
         ) x
   where x.dr_no_ext not in (64, 66)
   union
   SELECT DISTINCT
       drname.dr_id as dr_id,
       drname.dr_no_ext as provider_no,
       TRAIN.CD AS group_cd,
       HOSPITAL2.txt AS group_desc,
       ' ' as sch_type , ' ' as primary_practice_ind
   FROM drname
   INNER JOIN TRAIN ON drname.dr_id = train.dr_id
   inner  JOIN HOSPITAL as HOSPITAL2
   ON len(TRAIN.CD) > 0  and HOSPITAL2.cd = TRAIN.CD
   and HOSPITAL2.fac_cd = TRAIN.fac_cd
   and lower(substring (HOSPITAL2.txt, 1, 6)) <> 'cancer'
   WHERE LEN(drname.dr_no_ext) <7
   and drname.verfac = 'false'
   and drname.dr_lname <> 'Test'
   and TRAIN.CD  like 'PRS%'
   order by  1, 2
)


select dr_id as src_personnel_id,
       provider_no,
       group_cd,
       group_desc,
       primary_practice_ind
from grouptbl