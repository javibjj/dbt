with drname as (
    select * from drname where LENGTH('dr_no_ext') = 6 and REGEXP_LIKE('dr_no_ext','^[0-9]*$') and dr_lname != 'Test'
),



select
    dr.dr_id as src_personnel_id,
    dr.dr_no_ext as provider_no,
    'ECHO' as src_sys_cd,
    TRIM(addr2),
    TRIM(city),
    TRIM(state),
    TRIM(zip),
    TRIM(phone),
    TRIM(fax),
    case when sch_type='1' then 'Y' else 'N' as primary_practice_ind,
    case when addr is null then 'N/A' else addr
from drname dr
left join address1 a1
on dr.dr_id=a1.dr_id
