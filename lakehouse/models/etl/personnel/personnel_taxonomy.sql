with drname as (
    select * from drname where LENGTH('dr_no_ext') = 6 and REGEXP_LIKE('dr_no_ext','^[0-9]*$') and dr_lname != 'Test'
),


select
    dr.dr_id as src_personnel_id,
    dr.dr_no_ext as provider_no,
    taxarr.descript as descript
from
    drname dr,
left join taxonomy tax
on dr.dr_id=tax.dr_id
left join taxo_arr taxarr
on TRIM(tax.cd) = TRIM(taxarr.cd)