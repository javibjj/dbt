with drname as (
    select * from drname where LENGTH('dr_no_ext') = 6 and REGEXP_LIKE('dr_no_ext','^[0-9]*$') and dr_lname != 'Test'
),


select

h.txt as organization_affiliation_name,
aa.txt as organization_affiliation_type


from drname dr,
left join train t
on dr.dr_id = t.dr_id
left join hospital h
on t.cd = h.cd
join affi_arr aa
t.sch_type = aa.cd

order by h.txt, t.fdate
