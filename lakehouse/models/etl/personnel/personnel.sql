with drname as (
    select * from drname where LENGTH('dr_no_ext') = 6 and REGEXP_LIKE('dr_no_ext','^[0-9]*$') and dr_lname != 'Test'
),

with language as (
   select drl.dr_id, LISTAGG(lt.txt,',') as language
   from dr_lan drl
   left join lan_tab lt
   on drl.cd=lt.cd
   group by drl.dr_id
)


select
    'ECHO' as src_sys_cd,
    dr.dr_id as src_personnel_id,
    dr.dr_no_ext as provider_no,
    dr_lname || ' ' || ' ' || drtitle || ',' || ' ' || dr_fname || ' ' ||  SUBSTR(dr_iname, 1, 1) || '' || drsuffix as personnel_full_name,
    TO_DATE(birthdate, 'MM/dd/yyyy') as birthdate,
    case when dr.ud1 is null or dr.ud1='' then 'N/A' else dr.ud1 end as ud1,
    case when dr.ud7 is null or dr.ud7='' then 'N' else dr.ud7 end as ud7,
    case when UPPER(dr.drtitle) in ('MD', 'DO', 'PA', 'PA_C', 'APRN') then 'Y' else 'N' end as provider_ind,
    '' as position_description,
    case when dr.pict1 is null then 'N/A' else REGEXP_SUBSTR(TRIM(SPLIT(LOWER(split("pict1", "photos/")[1]), ".jpg")[0]), "\\d+", 0) end as badge_id,
    UPPER(dr_lname),
    UPPER(dr_fname),
    UPPER(dr_iname),
    case when cd.sch_type='34' then lic_num else null end as flme_no
    case when cd.sch_type='07' then lic_num else null end as dea_number,
    case when UPPER(fac_cd)='BHQN' and UPPER(active123)='BHP' then 'Y' else 'N' end as bhqn_flag
from
drname dr
left join credent cd
on dr.dr_id=cd.dr_id
left join stfstatus stf
on dr.dr_id=stf.dr_id
left join rollupspec roll
on dr.dr_no_ext=roll.physicial_number
left join spec_tab st
on roll.corp_rollup_speciality=st.txt
left join language lan
on dr.dr_id=lan.dr_id

