with drname as (
    select * from drname where LENGTH('dr_no_ext') = 6 and REGEXP_LIKE('dr_no_ext','^[0-9]*$') and dr_lname != 'Test'
),

select

'ECHO' as src_sys_cd,
dr.dr_id as src_personnel_id,
dr.dr_no_ext as provider_no,
case when sh1.txt is null then sh2.txt else sh1.txt end as education_institute_name,
eduarr.txt as education_institute_type,
lic.txt as credential_license_type,
sl.txt as credential_license_name,
edu.fdate as education_from_date,
edu.tdate as education_to_date,
cr.issue_dt  as credential_license_issue_date,
cr.lic_date as credential_license_valid_until_date


from drname dr
left join educate edu
on dr.dr_id = edu.dr_id
left join school sh1
on edu.cd = sh1.cd and edu.fac_cd = sh1.fac_cd
left join school sh2
on edu.cd = sh2.cd and sh2.fac_cd = ''
left join edu_arr eduarr
on edu.sch_type = eduarr.cd
left join credent cr
on dr.dr_id = cr.dr_id
left join lic_arr lic
on cr.sch_type = lic.cd
left join statelic sl
on cr.cd = sl.cd
where LENGTH(edu.sch_type) > 0