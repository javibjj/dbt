# Setting the S3 bucket URI:
S3_CODE_BUCKET_DIR=s3://dbtdbttesttbuck/DBT/lakehouse/

# Setting the secret name from AWS secret manager:
SFLK_INFO="DBT_sects"

# Setting the current directory:
cd $DBT_lakehouse
echo -e "\n########## Present working directory ##########"
pwd

#Copying files from S3:
echo -e "\n########## Copying files from S3 - ${S3_CODE_BUCKET_DIR} ##########"
aws s3 cp --recursive ${S3_CODE_BUCKET_DIR} .

#Installing DBT dependencies:
echo -e "\n########## Installing DBT dependencies ##########"
dbt deps

#For executing runtime DBT commands:
echo $1 | sed "s/,/\n/g" >> dbt_cmds.sh
chmod 700 ./dbt_cmds.sh

#Retrieving snowflake information:
echo -e "\n########## Retrieving snowflake connecting info ##########"
SFLK_INFO_PARSED=`python3 ${DBT_HOME}/retrieve_secrets.py ${SFLK_INFO}`
#echo "SFLK INFO_PARSED : $SFLK_INFO_PARSED "

#Setting the variables:
export DBT_SNOWFLAKE_ACCOUNT=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_ACCOUNT"`
export DBT_SNOWFLAKE_USER=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_USER"`
export DBT_SNOWFLAKE_PASSWORD=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_PASSWORD"`
export DBT_SNOWFLAKE_DATABASE=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_DATABASE"`
export DBT_SNOWFLAKE_SCHEMA=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_SCHEMA"`
export DBT_SNOWFLAKE_WH=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_WH"`
export DBT_SNOWFLAKE_ACCOUNT_ROLE=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_ACCOUNT_ROLE"`

#Setting DBT and Snowflake connections:
echo -e "\n########## Checking DBT-Snowflake connection ##########"
dbt debug

#Performing runtime DBT commands:
echo -e "\n########## Executing the following DBT commands ##########"
cat dbt_cmds.sh
echo -e "\n"
./dbt_cmds.sh
