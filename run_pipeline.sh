/*SFLK_INFO="DBT_sects"
#Retrieving snowflake information:
echo -e "\nRetrieving snowflake connecting info:"
SFLK_INFO_PARSED=`python3 ${DBT_HOME}/retrieve_secrets.py ${SFLK_INFO} | grep "__SECRETS__" | sed 's/__SECRETS__//g'`
#echo "SFLK INFO_PARSED : $SFLK_INFO_PARSED "

#Setting the variables:
export account=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_ACCOUNT"`
export user=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_USER"`
export password=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_PASSWORD"`
export database=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_DATABASE"`
export schema=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_SCHEMA"`
export threads=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "threads"`
export warehouse=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_WH"`
export role=`python3 ${DBT_HOME}/extract_json_key.py  "$SFLK_INFO_PARSED" "DBT_SNOWFLAKE_ACCOUNT_ROLE"`
*/
#Creating profiles.yml file
echo -e "connection:" > $DBT_PROFILES_DIR/profiles.yml
echo -e "  target: dev" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "  outputs:" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "    dev:" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      type: snowflake" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      account: bnb91573.us-east-1" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      user: jjavib" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      password: Admin@123" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      database: EDW_DEV" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      schema: STG" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      threads: 8" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      warehouse: COMPUTE_WH" >> $DBT_PROFILES_DIR/profiles.yml
echo -e "      role: ACCOUNTADMIN" >> $DBT_PROFILES_DIR/profiles.yml
